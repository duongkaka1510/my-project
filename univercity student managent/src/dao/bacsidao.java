package dao;

import java.awt.dnd.DnDConstants;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.bacsi;

public class bacsidao {
	//goi bang tt len
	public ArrayList<bacsi>laythongtin() throws Exception{
		ketnoi mKetnoi=new ketnoi();
		mKetnoi.KetNoi();
		String sql="select *from BAC_SI";
		PreparedStatement cmd=mKetnoi.cnn.prepareStatement(sql);
		ResultSet resultSet=cmd.executeQuery();
		ArrayList<bacsi>list=new ArrayList<>();
		while(resultSet.next()){
		String mbs=resultSet.getString("maBacSi");
		String tbs=resultSet.getString("tenBacSi");
		String gioitinh=resultSet.getString("gioiTinh");
	    String namsinh=resultSet.getString("namSinh");
		String quequan=resultSet.getString("queQuan");
		String mk=resultSet.getString("maKhoa");
		String mba=resultSet.getString("maBenhAn");
		list.add(new bacsi(mbs, tbs, gioitinh, namsinh, quequan, mk, mba));
		}
		return list;

	}
	// sua bac si
	public String suabs(String mabs,String tenbs,String gioitinh,String namsinh,String que,String mk,String maba) throws Exception {
		ketnoi scKetnoi=new ketnoi();
		scKetnoi.KetNoi();
		String sql="update BAC_SI "+"SET[tenBacSi]=?,[gioiTinh]=?,"+
				"[namSinh]=?,[queQuan]=?,[maKhoa]=?,[maBenhAn]=? "+ "where [maBacSi]=?";
		PreparedStatement preparedStatement=scKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, tenbs);
		preparedStatement.setString(2, gioitinh);
		preparedStatement.setString(3, namsinh);
		preparedStatement.setString(4, que);
		preparedStatement.setString(5, mk);
		preparedStatement.setString(6, maba);
		preparedStatement.setString(7, mabs);
		preparedStatement.executeUpdate();
		return "sua thanh cong";
	} 
	public String xoabs(String mabs) throws Exception {
		ketnoi xKetnoi=new ketnoi();
		xKetnoi.KetNoi();
		String sql="DELETE BAC_SI WHERE [maBacSi]=?";
		PreparedStatement preparedStatement=xKetnoi.cnn.prepareStatement(sql);
//		preparedStatement.setString(1, tenbs);
//		preparedStatement.setString(2, gioitinh);
//		preparedStatement.setString(3, namsinh);
//		preparedStatement.setString(4, que);
//		preparedStatement.setString(5, mk);
//		preparedStatement.setString(6, maba);
		preparedStatement.setString(1, mabs);
		preparedStatement.executeUpdate();
		return "xoa thanh cong";
		
	}
	public String thembs(String mabs,String tenbs,String gioitinh,String namsinh,String que,String mk,String maba) throws Exception {
		ketnoi xKetnoi=new ketnoi();
		xKetnoi.KetNoi();
		String sql="insert into BAC_SI values (?,?,?,?,?,?,?)";
		PreparedStatement preparedStatement=xKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, mabs);
		preparedStatement.setString(2, tenbs);
	    preparedStatement.setString(3, gioitinh);
	    preparedStatement.setString(4, namsinh);
		preparedStatement.setString(5, que);
		preparedStatement.setString(6, mk);
		preparedStatement.setString(7, maba);
		
		preparedStatement.executeUpdate();
		return "them thanh cong";
		
	}
		
}

