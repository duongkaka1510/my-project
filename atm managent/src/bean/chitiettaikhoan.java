package bean;

public class chitiettaikhoan {
	String id;
	String ngayruttien;
	int sotienrut;
	int soTaiKhoan;
	String ghichu;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNgayruttien() {
		return ngayruttien;
	}
	public void setNgayruttien(String ngayruttien) {
		this.ngayruttien = ngayruttien;
	}
	public int getSotienrut() {
		return sotienrut;
	}
	public void setSotienrut(int sotienrut) {
		this.sotienrut = sotienrut;
	}
	public int getSoTaiKhoan() {
		return soTaiKhoan;
	}
	public void setSoTaiKhoan(int soTaiKhoan) {
		this.soTaiKhoan = soTaiKhoan;
	}
	public String getGhichu() {
		return ghichu;
	}
	public void setGhichu(String ghichu) {
		this.ghichu = ghichu;
	}
	public chitiettaikhoan(String id, String ngayruttien, int sotienrut, int soTaiKhoan, String ghichu) {
		super();
		this.id = id;
		this.ngayruttien = ngayruttien;
		this.sotienrut = sotienrut;
		this.soTaiKhoan = soTaiKhoan;
		this.ghichu = ghichu;
	}
	

}
