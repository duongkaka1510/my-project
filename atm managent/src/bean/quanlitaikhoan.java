package bean;

public class quanlitaikhoan {
	int sotaikhoan;
	String hovaten;
	int sotien;
	String matkhau;
	int status;
	public int getSotaikhoan() {
		return sotaikhoan;
	}
	public void setSotaikhoan(int sotaikhoan) {
		this.sotaikhoan = sotaikhoan;
	}
	public String getHovaten() {
		return hovaten;
	}
	public void setHovaten(String hovaten) {
		this.hovaten = hovaten;
	}
	public int getSotien() {
		return sotien;
	}
	public void setSotien(int sotien) {
		this.sotien = sotien;
	}
	public String getMatkhau() {
		return matkhau;
	}
	public void setMatkhau(String matkhau) {
		this.matkhau = matkhau;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public quanlitaikhoan(int sotaikhoan, String hovaten, int sotien, String matkhau, int status) {
		super();
		this.sotaikhoan = sotaikhoan;
		this.hovaten = hovaten;
		this.sotien = sotien;
		this.matkhau = matkhau;
		this.status = status;
	}

}
