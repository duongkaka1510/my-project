package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.quanlitaikhoan;

public class taikhoandao {
	public ArrayList<quanlitaikhoan>laythongtin() throws Exception{
		ketnoi tKetnoi=new ketnoi();
		tKetnoi.KetNoi();
		String sql="select *from TaiKhoan";
		PreparedStatement preparedStatement=tKetnoi.cnn.prepareStatement(sql);
		ResultSet resultSet=preparedStatement.executeQuery();
		ArrayList<quanlitaikhoan>list=new ArrayList<>();
		while(resultSet.next()) {
			int stk=resultSet.getInt("soTaiKhoan");
			String hoten=resultSet.getString("hoTen");
			int soTien=resultSet.getInt("soTien");
			String mk=resultSet.getString("matKhau");
			int status=resultSet.getInt("status");
			list.add(new quanlitaikhoan(stk, hoten, soTien, mk, status));
			
		}
		return list;
		
	}
	public String them(int sotk,String hoten,int sotien,String mkhau,int status) throws Exception {
		ketnoi tKetnoi=new ketnoi();
		tKetnoi.KetNoi();
		String sql="insert into TaiKhoan values (?,?,?,?,?)";
		PreparedStatement preparedStatement=tKetnoi.cnn.prepareStatement(sql);	
		preparedStatement.setInt(1,sotk);
		preparedStatement.setString(2, hoten);
		preparedStatement.setInt(3, sotien);
		preparedStatement.setString(4, mkhau);
		preparedStatement.setInt(5, status);
		
		preparedStatement.executeUpdate();
		return "them thanh cong";
		
	}
	public String sua(int sotk,String hoten,int sotien,String matkhau,int status)
			throws Exception {
		ketnoi sKetnoi=new ketnoi();
		sKetnoi.KetNoi();
		String sql="update TaiKhoan set [hoTen]=?,[soTien]=?,[matKhau]=?,[status]=? "+
		" where[soTaiKhoan]=?";
		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, hoten);
		preparedStatement.setInt(2, sotien);
		preparedStatement.setString(3, matkhau);
		preparedStatement.setInt(4, status);
		preparedStatement.setInt(5, sotk);
		preparedStatement.executeUpdate();
		return "sua thanh cong";
		
	}
	public String xoa(int sotk) throws Exception {
		ketnoi xKetnoi=new ketnoi();
		xKetnoi.KetNoi();
		String sql="DELETE TaiKhoan WHERE [soTaiKhoan]=?";
		PreparedStatement preparedStatement=xKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setInt(1, sotk);
		preparedStatement.executeUpdate();
		return "xoa thanh cong";
		
	}
	public String suasotien(int sotk,int sotien) throws Exception {
		ketnoi sKetnoi=new ketnoi();
		sKetnoi.KetNoi();
		String sql="update TaiKhoan set [soTien]=? WHERE [soTaiKhoan]=?";
		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setInt(1, sotien);
		preparedStatement.setInt(2, sotk);
		preparedStatement.executeUpdate();
		return "";
		
	}
	
	public String themtkct(String ngayRutTien,int sotienrutra,int soTaiKhoan) throws Exception {
		ketnoi tKetnoi=new ketnoi();
		tKetnoi.KetNoi();
		String sql="insert into ChiTietTaiKhoan values (?,?,?)";
		PreparedStatement preparedStatement=tKetnoi.cnn.prepareStatement(sql);
		//preparedStatement.setString(1, id);
		preparedStatement.setString(1, ngayRutTien);
		preparedStatement.setInt(2, sotienrutra);
		preparedStatement.setInt(3, soTaiKhoan);
	//	preparedStatement.setString(5, ghichu);
		preparedStatement.executeUpdate();
		
		
		
		return "";
		
		
	

}
}

