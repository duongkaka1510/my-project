package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.chitiettaikhoan;

public class chitiettaikhoandao {
	public ArrayList<chitiettaikhoan>laythongtin() throws Exception{
		ketnoi cKetnoi=new ketnoi();
		cKetnoi.KetNoi();
		String sql="select *from ChiTietTaiKhoan";
		PreparedStatement preparedStatement=cKetnoi.cnn.prepareStatement(sql);
		ResultSet resultSet=preparedStatement.executeQuery();
		ArrayList<chitiettaikhoan>list=new ArrayList<>();
		while(resultSet.next()) {
			String id=resultSet.getString("id");
			String ngayrut=resultSet.getString("ngayRutTien");
			int sotienrut=resultSet.getInt("soTienRutRa");
			int sotk=resultSet.getInt("soTaiKhoan");
			String ghichu=resultSet.getString("ghiChi");
			//preparedStatement.executeLargeUpdate();
			list.add(new chitiettaikhoan(id, ngayrut, sotienrut, sotk, ghichu));
		}
		return list;
		
	}
	public String suattcanhan(int sotk,String hoten,String mk) throws Exception {
		ketnoi sKetnoi=new ketnoi();
		sKetnoi.KetNoi();
		String sql="update TaiKhoan set [hoTen]=?,[matKhau]=? "+" where [soTaiKhoan]=?";
		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, hoten);
		preparedStatement.setString(2, mk);
		preparedStatement.setInt(3, sotk);
		preparedStatement.executeUpdate();
		
		return "sua thanh cong";
		
	}
	//user: chi tiết tài khoản: SoTienRutRa:int SoTaiKhoan:intGhichu:String
//	public String themcttk(String id,String tgian,int sotienrutra,int sotk,String ghichu) throws Exception {
//		ketnoi tKetnoi=new ketnoi();
//		tKetnoi.KetNoi();
//		String sql="update ChiTietTaiKhoan set [ngayRutTien]=?,[soTienRutRa]=?,[soTaiKhoan]=?,[ghiChi]=? where [id]=?";
//		PreparedStatement preparedStatement=tKetnoi.cnn.prepareStatement(sql);
//		preparedStatement.setString(1,"ngayRutTien");
//		preparedStatement.setInt(2,"soTienRutRa");
//		preparedStatement.setInt(3,"soTaiKhoan");
//		preparedStatement.setString(4, "ghiChi");
//		preparedStatement.setString(5, "id");
//		preparedStatement.executeUpdate();
//		return "";
	public String themtkct(String id,String ngayRutTien,int sotienrutra,int soTaiKhoan,String ghichu) throws Exception {
		ketnoi tKetnoi=new ketnoi();
		tKetnoi.KetNoi();
		String sql="insert into ChiTietTaiKhoan values (?,?,?,?,?)";
		PreparedStatement preparedStatement=tKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, id);
		preparedStatement.setString(2, ngayRutTien);
		preparedStatement.setInt(3, sotienrutra);
		preparedStatement.setInt(4, soTaiKhoan);
		preparedStatement.setString(5, ghichu);

		preparedStatement.executeUpdate();
		
		
		return "";
		
	}
		
	
	

}
