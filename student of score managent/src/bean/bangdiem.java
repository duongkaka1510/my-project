package bean;

public class bangdiem {
	int stt;
	String maSv;
	int diemtoan;
	int diemLy;
	int diemHoa;
	public int getStt() {
		return stt;
	}
	public void setStt(int stt) {
		this.stt = stt;
	}
	public String getMaSv() {
		return maSv;
	}
	public void setMaSv(String maSv) {
		this.maSv = maSv;
	}
	public int getDiemtoan() {
		return diemtoan;
	}
	public void setDiemtoan(int diemtoan) {
		this.diemtoan = diemtoan;
	}
	public int getDiemLy() {
		return diemLy;
	}
	public void setDiemLy(int diemLy) {
		this.diemLy = diemLy;
	}
	public int getDiemHoa() {
		return diemHoa;
	}
	public void setDiemHoa(int diemHoa) {
		this.diemHoa = diemHoa;
	}
	public bangdiem(int stt, String maSv, int diemtoan, int diemLy, int diemHoa) {
		super();
		this.stt = stt;
		this.maSv = maSv;
		this.diemtoan = diemtoan;
		this.diemLy = diemLy;
		this.diemHoa = diemHoa;
	}
}
