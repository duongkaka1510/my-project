package bean;

public class ThanhVien {
	String maSV;
	String ten;
	String mk;
	int status;
	public ThanhVien(String maSV, String ten, String mk, int status) {
		super();
		this.maSV = maSV;
		this.ten = ten;
		this.mk = mk;
		this.status = status;
	}
	public String getMaSV() {
		return maSV;
	}
	public void setMaSV(String maSV) {
		this.maSV = maSV;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public String getMk() {
		return mk;
	}
	public void setMk(String mk) {
		this.mk = mk;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

}
