package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.DiemTrungBinh;
import bean.bangdiem;

public class BangDiemDao {
	public ArrayList<bangdiem>laythongtin() throws Exception {
		ketnoi xKetnoi=new ketnoi();
		xKetnoi.KetNoi();
		String sql="select *from BangDiemSV";
		PreparedStatement preparedStatement=xKetnoi.cnn.prepareStatement(sql);
		ResultSet resultSet=preparedStatement.executeQuery();
		ArrayList<bangdiem>list=new ArrayList<>();
		while(resultSet.next()) {
			int stt=resultSet.getInt("STT");
			String msv=resultSet.getString("maSinhVien");
			int diemtoan=resultSet.getInt("diemToan");
			int diemly=resultSet.getInt("diemLy");
			int diemHoa=resultSet.getInt("diemHoa");
			list.add(new bangdiem(stt, msv, diemtoan, diemly, diemHoa));
			
		}
		return list;
		
	}
	public String themDiem(int STT,String maSV,int diemToan,int diemLy,int diemHoa) throws Exception {
		ketnoi xKetnoi=new ketnoi();
		xKetnoi.KetNoi();
		String sql="insert into BangDiemSV values (?,?,?,?,?) ";
		PreparedStatement preparedStatement=xKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setInt(1, STT);
		preparedStatement.setString(2, maSV);
		preparedStatement.setInt(3, diemToan);
		preparedStatement.setInt(4, diemLy);
		preparedStatement.setInt(5, diemHoa);
		preparedStatement.executeUpdate();
		
		return "sua thanh cong";
		
		
	}
	public String suaDiem(int stt,String maSV,int diemToan,int diemLy,int diemHoa) throws Exception {
		ketnoi xKetnoi=new ketnoi();
		xKetnoi.KetNoi();
		String sql="update BangDiemSV set ([maSinhVien]=?,[diemToan]=?,[diemLy]=?,[diemHoa]=?) where [STT]=?";
		PreparedStatement  preparedStatement=xKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, maSV);
		preparedStatement.setInt(3, diemToan);
		preparedStatement.setInt(3, diemLy);
		preparedStatement.setInt(3, diemHoa);
		preparedStatement.setInt(4, stt);
		preparedStatement.executeUpdate();
		
		return "sua thanh cong";
	}
	public String xoadiem(int stt) throws Exception {
		ketnoi xKetnoi=new ketnoi();
		xKetnoi.KetNoi();
		String sql="delete BangDiemSV where [STT]=?";
		PreparedStatement preparedStatement=xKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setInt(1, stt);
		preparedStatement.executeUpdate();
		return "xoa thanh cong";
		
		
	}
	public ArrayList<DiemTrungBinh>sapxep() throws Exception {
		ketnoi xKetnoi=new ketnoi();
		xKetnoi.KetNoi();
		
		return null;
		
	}

}
