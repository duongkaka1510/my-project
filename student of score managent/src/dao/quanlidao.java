package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.ThanhVien;

public class quanlidao {
	public ArrayList<ThanhVien>laythongtin() throws Exception{
		ketnoi xKetnoi=new ketnoi();
		xKetnoi.KetNoi();
		String sql="select *from ThanhVien";
		PreparedStatement preparedStatement=xKetnoi.cnn.prepareStatement(sql);
		ResultSet resultSet=preparedStatement.executeQuery();
		ArrayList<ThanhVien>list=new ArrayList<>();
		while(resultSet.next()) {
			String maSV=resultSet.getString("maSinhVien");
			String ten=resultSet.getString("ten");
			String matK=resultSet.getString("matKhau");
			int status=resultSet.getInt("status");
			list.add(new ThanhVien(maSV, ten, matK, status));
		}
		return list;
		
	}
	public String themTv(String maSV,String ten,String matK,int status) throws Exception {
		ketnoi xKetnoi=new ketnoi();
		xKetnoi.KetNoi();
		String sql="insert into ThanhVien values (?,?,?,?)";
		PreparedStatement preparedStatement=xKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, maSV);
		preparedStatement.setString(2, ten);
		preparedStatement.setString(3, matK);
		preparedStatement.setInt(4, status);
		preparedStatement.executeUpdate();
		return "them thanh cong";
		
	}
	public String suaTV(String maSV,String ten,String mk,int status) throws Exception {
		ketnoi xlKetnoi=new ketnoi();
		xlKetnoi.KetNoi();
		String sql="update ThanhVien set [ten]=?,[matKhau]=?,[status]=? where [maSinhVien]=?";
		PreparedStatement preparedStatement=xlKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, ten);
		preparedStatement.setString(2, mk);
		preparedStatement.setInt(3, status);
		preparedStatement.setString(4, maSV);
		preparedStatement.executeUpdate();
		
		return "sua thanh cong";
		
	}
	public String xoa(String msv) throws Exception {
		ketnoi xKetnoi=new ketnoi();
		xKetnoi.KetNoi();
		String sql="delete ThanhVien where [maSinhVien]=?";
		PreparedStatement preparedStatement=xKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, msv);
		preparedStatement.executeUpdate();
		return "xoa thanh cong";
		
		
	}

}
