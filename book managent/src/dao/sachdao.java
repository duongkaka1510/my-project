package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.sach;

public class sachdao {
	public ArrayList<sach>laythongtin() throws Exception{
		ketnoi sKetnoi=new ketnoi();
		sKetnoi.KetNoi();
		String sql="select *from Sach";
		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
		ResultSet resultSet=preparedStatement.executeQuery();
		ArrayList<sach>list1=new ArrayList<>();
		while(resultSet.next()) {
		String masach=	resultSet.getString("maSach");
		String tensach=	resultSet.getString("tenSach");
		int dongia=	resultSet.getInt("donGia");
			list1.add(new sach(masach, tensach, dongia));
		}
		return list1;
		
	}
	public String themsach(String masach,String tensach,int dongia) throws Exception {
		ketnoi tKetnoi=new ketnoi();
		tKetnoi.KetNoi();
		String sql="insert into Sach values (?,?,?)";
		PreparedStatement preparedStatement=tKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, masach);
		preparedStatement.setString(2, tensach);
		preparedStatement.setInt(3, dongia);
		preparedStatement.executeUpdate();
		
		return "them sach thanh cong";
		
	}
	public String xoasach(String masach) throws Exception {
		ketnoi xKetnoi=new ketnoi();
		xKetnoi.KetNoi();
		String sql="delete Sach where [maSach]=?";
		PreparedStatement preparedStatement=xKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, masach);
		preparedStatement.executeUpdate();
		return "xoa sach thanh cong";
		
	}
	public String suasach(String masach,String tensach,int gia) throws Exception {
		ketnoi sKetnoi=new ketnoi();
		sKetnoi.KetNoi();
		String sql="update Sach set [tenSach]=?,[donGia]=? where [maSach]=?";
		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, tensach);
		preparedStatement.setInt(2, gia);
		preparedStatement.setString(3, masach);
		preparedStatement.executeUpdate();
		
		return "sua sach thanh cong";
		
		
	}

}
