package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import bean.nhanvien;

public class nhanviendao {
	public ArrayList<nhanvien>laythongtin() throws Exception{
		ketnoi qKetnoi=new ketnoi();
		qKetnoi.KetNoi();
		String sql="select *from NhanVien";
		PreparedStatement preparedStatement=qKetnoi.cnn.prepareStatement(sql);
		ResultSet resultSet=preparedStatement.executeQuery();
		ArrayList<nhanvien>list=new ArrayList<>();
		while(resultSet.next()){
			int manv=	resultSet.getInt("maNhanVien");
			String tennv=	resultSet.getString("tenNhanVien");
			int luong=	resultSet.getInt("tienLuong");
			String mk=	resultSet.getString("matKhau");
			String masach=	resultSet.getString("maSach");
			int status=	resultSet.getInt("status");
		
			list.add(new nhanvien(manv, tennv, luong, mk, masach, status));
		}
		return list;
		
	}
	public String them(int manv,String tennv,int tienluong,String matkhau,String masach,int status) throws Exception {
		ketnoi cKetnoi=new ketnoi();
		cKetnoi.KetNoi();
		String sql="insert into Nhanvien values (?,?,?,?,?,?)";
		PreparedStatement preparedStatement=cKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setInt(1, manv);
		preparedStatement.setString(2, tennv);
		preparedStatement.setInt(3, tienluong);
		preparedStatement.setString(4, matkhau);
		preparedStatement.setString(5, masach);
		preparedStatement.setInt(6, status);
		preparedStatement.executeUpdate();
				
		return "them thanh cong";
		
	}
	public String sua(int manv,String tennv,int tienluong,String matkhau,String masach,int status) throws Exception {
		ketnoi sKetnoi=new ketnoi();
		sKetnoi.KetNoi();
		String sql="update NhanVien set [tenNhanVien]=?,[tienLuong]=?,[matKhau]=?,[maSach]=?,[status]=?  where [maNhanVien]=?";
		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, tennv);
		preparedStatement.setInt(2, tienluong);
		preparedStatement.setString(3, matkhau);
		preparedStatement.setString(4, masach);
		preparedStatement.setInt(5, status);
		preparedStatement.setInt(6, manv);
		preparedStatement.executeUpdate();
		
		return "sua thanh cong";
		
		
	}
	public String xoa(int manv) throws Exception {
		ketnoi sKetnoi=new ketnoi();
		sKetnoi.KetNoi();
		String sql="delete NhanVien where [maNhanVien]=?";
		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setInt(1, manv);
		preparedStatement.executeUpdate();
		return "xoa thanh cong";
		
	}

}
