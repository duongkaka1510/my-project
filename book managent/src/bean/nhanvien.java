package bean;

public class nhanvien {
	int manhanvien;
	String tennhanvien;
	int tienluong;
	String matkhau;
	String masach;
	int status;
	public int getManhanvien() {
		return manhanvien;
	}
	public void setManhanvien(int manhanvien) {
		this.manhanvien = manhanvien;
	}
	public String getTennhanvien() {
		return tennhanvien;
	}
	public void setTennhanvien(String tennhanvien) {
		this.tennhanvien = tennhanvien;
	}
	public int getTienluong() {
		return tienluong;
	}
	public void setTienluong(int tienluong) {
		this.tienluong = tienluong;
	}
	public String getMatkhau() {
		return matkhau;
	}
	public void setMatkhau(String matkhau) {
		this.matkhau = matkhau;
	}
	public String getMasach() {
		return masach;
	}
	public void setMasach(String masach) {
		this.masach = masach;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public nhanvien(int manhanvien, String tennhanvien, int tienluong, String matkhau, String masach, int status) {
		super();
		this.manhanvien = manhanvien;
		this.tennhanvien = tennhanvien;
		this.tienluong = tienluong;
		this.matkhau = matkhau;
		this.masach = masach;
		this.status = status;
	}

}
