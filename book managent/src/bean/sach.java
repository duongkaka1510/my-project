package bean;

public class sach {
	String masach;
	String tensach;
	int dongia;
	public String getMasach() {
		return masach;
	}
	public void setMasach(String masach) {
		this.masach = masach;
	}
	public String getTensach() {
		return tensach;
	}
	public void setTensach(String tensach) {
		this.tensach = tensach;
	}
	public int getDongia() {
		return dongia;
	}
	public void setDongia(int dongia) {
		this.dongia = dongia;
	}
	public sach(String masach, String tensach, int dongia) {
		super();
		this.masach = masach;
		this.tensach = tensach;
		this.dongia = dongia;
	}

}
