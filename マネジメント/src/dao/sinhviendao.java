package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.sinhvien;

public class sinhviendao {
	public ArrayList<sinhvien>laythongtin() throws Exception{
		ketnoi scKetnoi=new ketnoi();
		scKetnoi.KetNoi();
		String spl="select *from quanlisinhvien1";
		PreparedStatement preparedStatement=scKetnoi.cnn.prepareStatement(spl);
		ResultSet resultSet=preparedStatement.executeQuery();
		ArrayList<sinhvien>list=new ArrayList<>();
		while(resultSet.next()) {
			int maSV =resultSet.getInt("mSV");
			String tenSV=resultSet.getString("tenSV");
			int tuoiSV=resultSet.getInt("tuoiSV");
			int namSinh=resultSet.getInt("namSinhSV");
			String mksv=resultSet.getString("mksv");
			list.add(new sinhvien(maSV, tenSV, tuoiSV, namSinh, mksv));
		}
		return list;
		
	}
	public String themsv(int msv,String tensv,int tuoisv,int namsinh,String mksv) throws Exception {
		ketnoi scKetnoi=new ketnoi();
		scKetnoi.KetNoi();
		String sql="insert into quanlisinhvien1 values (?,?,?,?,?)";
		PreparedStatement preparedStatement=scKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setInt(1, msv);
		preparedStatement.setString(2, tensv);
		preparedStatement.setInt(3,tuoisv );
		preparedStatement.setInt(4, namsinh);
		preparedStatement.setString(5, mksv);
		preparedStatement.executeUpdate();
		
		return "them sv thanh cong";
		
	}
	public String suasv(int msv,String tensv,int tuoisv,int namsinh,String mksv ) throws Exception {
		ketnoi mKetnoi=new ketnoi();
		mKetnoi.KetNoi();
		String sql="update quanlisinhvien1 "+ "set [tenSV]=?, [tuoiSV]=?, [namSinhSV]=?, [mksv]=? "+"where [mSV]=?";
		PreparedStatement preparedStatement=mKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1,tensv);
		preparedStatement.setInt(2,tuoisv);
		preparedStatement.setInt(3,namsinh);
		preparedStatement.setString(4,mksv);
		preparedStatement.setInt(5,msv);
		preparedStatement.executeUpdate();
		
		return "sua thanh cong";
		
	}
	public String xoasv(int msv) throws Exception {
		ketnoi mKetnoi=new ketnoi();
		mKetnoi.KetNoi();
		String sql="DELETE quanlisinhvien1 "+" where [mSV]=?";
		PreparedStatement preparedStatement=mKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setInt(1, msv);
		preparedStatement.executeUpdate();
		return "xoa thanh cong";
	}

}
