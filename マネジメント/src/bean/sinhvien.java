package bean;

public class sinhvien {
	int msv;
	String tenSV;
	int tuoiSV;
	int namSinh;
	String mksvString;
	public int getMsv() {
		return msv;
	}
	public void setMsv(int msv) {
		this.msv = msv;
	}
	public String getTenSV() {
		return tenSV;
	}
	public void setTenSV(String tenSV) {
		this.tenSV = tenSV;
	}
	public int getTuoiSV() {
		return tuoiSV;
	}
	public void setTuoiSV(int tuoiSV) {
		this.tuoiSV = tuoiSV;
	}
	public int getNamSinh() {
		return namSinh;
	}
	public void setNamSinh(int namSinh) {
		this.namSinh = namSinh;
	}
	public String getMksvString() {
		return mksvString;
	}
	public void setMksvString(String mksvString) {
		this.mksvString = mksvString;
	}
	public sinhvien(int msv, String tenSV, int tuoiSV, int namSinh, String mksvString) {
		super();
		this.msv = msv;
		this.tenSV = tenSV;
		this.tuoiSV = tuoiSV;
		this.namSinh = namSinh;
		this.mksvString = mksvString;
	}
	

}
