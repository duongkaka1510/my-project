package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.User;

public class userdao {
	public String themuser(int maTaiKhoan,String ten,String tuoi,String matkhau,int status) throws Exception {
		ketnoi kkKetnoi=new ketnoi();
		kkKetnoi.KetNoi();
		String sql="INSERT INTO TaiKhoan values (?,?,?,?,?)";
		PreparedStatement preparedStatement=kkKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setInt(1, maTaiKhoan);
		preparedStatement.setString(2, ten);
		preparedStatement.setString(3, tuoi);
		preparedStatement.setString(4, matkhau);
		preparedStatement.setInt(5, status);
		preparedStatement.executeUpdate();
		
		return "them thanh cong";
		
	}
	public String xoauser(int maTaiKhoan) throws Exception {
		ketnoi sKetnoi=new ketnoi();
		sKetnoi.KetNoi();
		String sql="delete TaiKhoan where [maTaiKhoan]=?";
		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setInt(1, maTaiKhoan);
		preparedStatement.executeUpdate();
		
		return "xoa thanh cong";
		
	}
	public String suaUser(int maTaikhoan,String ten,String tuoi,String matKhau,int status) throws Exception {
		ketnoi sKetnoi=new ketnoi();
		sKetnoi.KetNoi();
		String sql="update TaiKhoan set [ten]=?,[tuoi]=?,[matKhau]=?,[status]=? where [maTaiKhoan]=? ";
		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
		
		preparedStatement.setString(1, ten);
		preparedStatement.setString(2, tuoi);
		preparedStatement.setString(3, matKhau);
		preparedStatement.setInt(4, status);
		preparedStatement.setInt(5, maTaikhoan);
		preparedStatement.executeUpdate();
		return "sua thanh cong";
		
	}
	public ArrayList<User> laythongtin() throws Exception {
		ketnoi sKetnoi=new ketnoi();
		sKetnoi.KetNoi();
		String sql="SELECT *FROM TaiKhoan";
		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
		ResultSet resultSet=preparedStatement.executeQuery();
		ArrayList<User>list=new ArrayList<>();
		while(resultSet.next()) {
			int mataikhoan=resultSet.getInt("maTaiKhoan");
			String ten=resultSet.getString("ten");
			String tuoi=resultSet.getString("tuoi");
			String matkhau=resultSet.getString("matKhau");
			int status=resultSet.getInt("status");
			list.add(new User(mataikhoan, ten, tuoi, matkhau, status));
		}
		return list;
		
	}

}
