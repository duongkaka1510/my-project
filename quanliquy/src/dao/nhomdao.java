package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.swing.plaf.synth.SynthToolTipUI;

import bean.Nhom;

public class nhomdao {
	public ArrayList<Nhom>laythongtinnhom() throws Exception{
		ketnoi cKetnoi=new ketnoi();
		cKetnoi.KetNoi();
		String sql="select *from Nhom";
		PreparedStatement preparedStatement=cKetnoi.cnn.prepareStatement(sql);
		ResultSet resultSet=preparedStatement.executeQuery();
		ArrayList<Nhom>list=new ArrayList<>();
		while(resultSet.next()) {
			String manhom=resultSet.getString("maNhom");
			String tenNhom=resultSet.getString("tenNhom");
			int maTaiKhoan=resultSet.getInt("maTaiKhoan");
			
			list.add(new Nhom(manhom, tenNhom, maTaiKhoan));
			
		}
		return list;
		
	}
	public String themNhom(String maNhomString,String tenNhom,int maTaiKhoan) throws Exception {
		ketnoi sKetnoi=new ketnoi();
		sKetnoi.KetNoi();
		String sql="insert into Nhom values (?,?,?) ";
		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, maNhomString);
		preparedStatement.setString(2, tenNhom);
		preparedStatement.setInt(3, maTaiKhoan);
		preparedStatement.executeUpdate();
		return "them nhom thanh cong";
		
	}
	public String suaNhom(String manhom,String tenNhom,int maTaiKhoan) throws Exception {
		ketnoi sKetnoi=new ketnoi();
		sKetnoi.KetNoi();
		String sql="update Nhom set ([tenNhom]=?,[maTaiKhoan]=? where [maNhom]=?";
		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, tenNhom);
		preparedStatement.setInt(2, maTaiKhoan);
		preparedStatement.setNString(3, manhom);
		preparedStatement.executeUpdate();
		
		return "sua thanh cong";
		
	}
	public String xoaNhom(String maNhom) throws Exception {
		ketnoi sKetnoi=new ketnoi();
		sKetnoi.KetNoi();
		String sql="delete Nhom where [maNhom]=?";
		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
		preparedStatement.setString(1, maNhom);
		preparedStatement.executeUpdate();
				
		return "xoa thanh cong";
		
		
	}

}
