package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import bean.ThuChi;

public class ThuChidao {
 	public String suathuchi(String id_thuchi,int sotien,int mataikhoan,int ghichu) throws Exception {
 		ketnoi sKetnoi=new ketnoi();
 		sKetnoi.KetNoi();
 		String sql="insert into ThuChi values (?,?,?,?)";
 		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
 		preparedStatement.setString(1, id_thuchi);
 		preparedStatement.setInt(2, sotien);
 		preparedStatement.setInt(3, mataikhoan);
 		preparedStatement.setInt(4, ghichu);
 		preparedStatement.executeUpdate();
		return "giao dich thanh cong";
 		
 	}
 	public ArrayList<ThuChi>laythongtin() throws Exception{
 		ketnoi sKetnoi=new ketnoi();
 		sKetnoi.KetNoi();
 		String sql="select *from ThuChi";
 		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
 		ResultSet resultSet=preparedStatement.executeQuery();
 		ArrayList<ThuChi>list=new ArrayList<>();
 		while(resultSet.next()) {
 			String ndthuchi=resultSet.getString("id_thuCHi");
 			int sotien=resultSet.getInt("soTien");
 			int mataikhoan=resultSet.getInt("maTaiKhoan");
 			int ghichu=resultSet.getInt("ghiChu");
 			
 			list.add(new ThuChi(ndthuchi, sotien, mataikhoan, ghichu));
 		}
		
		return list;
 	}
 	public String suatien(String id_thuchi,int sotien) throws Exception {
 		ketnoi sKetnoi=new ketnoi();
 		sKetnoi.KetNoi();
 		String sql="update ThuChi set [soTien]=? where [id_thuChi]=?";
 		PreparedStatement preparedStatement=sKetnoi.cnn.prepareStatement(sql);
 		preparedStatement.setInt(1, sotien);
 		preparedStatement.setString(2, id_thuchi);
 		preparedStatement.executeUpdate();
		return "";
 		
 	}

}
