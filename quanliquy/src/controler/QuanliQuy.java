package controler;

import java.util.ArrayList;
import java.util.Scanner;

import bean.Nhom;
import bean.ThuChi;
import bean.User;
import dao.ThuChidao;
import dao.nhomdao;
import dao.userdao;

public class QuanliQuy {

	public static void main(String[] args) throws Exception {
		Scanner scanner = new Scanner(System.in);
		System.out.println("thuc hien dang nhap");
		System.out.println("nhap ma tai khoan");
		int nhapmakhoan = scanner.nextInt();
		scanner.nextLine();
		System.out.println("nhap mat khau");
		String nhapmk = scanner.nextLine();
		userdao u = new userdao();
		ArrayList<User> list = u.laythongtin();
		int check = 0;
		for (int i = 0; i < list.size(); i++) {
			if ((list.get(i).getMaTaiKhoan() == nhapmakhoan) && (list.get(i).getMatKhau().equals(nhapmk))
					&& (list.get(i).getSatatus() == 1)) {
				check = 1;
			} else if ((list.get(i).getMaTaiKhoan() == nhapmakhoan) && (list.get(i).getMatKhau().equals(nhapmk))
					&& (list.get(i).getSatatus() == 2)) {
				check = 2;
			}

		}
		if (check == 1) {
			System.out.println("quan li dang nhap thanh cong");
			System.out.println("nhan 1:them user");
			System.out.println("nhan 2:xoa user");
			System.out.println("nhan 3:sua user");
			System.out.println("nhan 4: hien thi user");
			System.out.println("nhan 5: them nhom, luu y 1 nhom toi da 10 user()");
			System.out.println("nhan 6:sua thong tin nhom");
			System.out.println("nhan 7:xoa nhom");
			System.out.println("nhan 8:hien thi nhom");
			System.out.println("chon chuc nang");
			int n = scanner.nextInt();
			int kt = 0;
			if (n == 1) {
				System.out.println("them ma tk");
				int matkthem = scanner.nextInt();
				for (int i = 0; i < list.size(); i++) {
					if (list.get(i).getMaTaiKhoan() == matkthem) {
						kt = 1;
					}
				}
				if (kt == 1) {
					System.out.println("ma tai khoan da ton tai, vui long nhap lai");

				} else {
					scanner.nextLine();
					System.out.println("nhap ten user");
					String tenString = scanner.nextLine();
					System.out.println("nhap tuoi");
					String tuoiString = scanner.nextLine();
					System.out.println("nhap mat khau");
					String mkString = scanner.nextLine();
					System.out.println("nhap status, luu y:1 la quan li,2 la user ");
					int status = scanner.nextInt();
					userdao userdao = new userdao();
					String themString = userdao.themuser(matkthem, tenString, tuoiString, mkString, status);
					System.out.println(themString);
				}
			} else if (n == 2) {
				System.out.println("ma tk can xoa");
				int matkxoa = scanner.nextInt();

				for (int i = 0; i < list.size(); i++) {
					if (list.get(i).getMaTaiKhoan() == matkxoa) {
						kt = 1;
					}
				}
				if (kt == 1) {
					userdao dUserdao = new userdao();
					String xoaString = dUserdao.xoauser(matkxoa);
					System.out.println(xoaString);
				} else {
					System.out.println("k tim thay ma tai khoan");
				}
			} else if (n == 3) {
				System.out.println("nhap tk can sua");
				int tksua = scanner.nextInt();
				for (int i = 0; i < list.size(); i++) {
					if (list.get(i).getMaTaiKhoan() == tksua) {
						kt = 1;
					}
					if (kt == 1) {
						userdao userdao = new userdao();
						scanner.nextLine();
						System.out.println("sua ten thanh");
						String suatenString = scanner.nextLine();
						System.out.println("sua tuoi thanh");
						String tuoiString = scanner.nextLine();
						System.out.println("sua mk thanh");
						String mkString = scanner.nextLine();

						System.out.println("sua status");
						int status = scanner.nextInt();
						String suaString = userdao.suaUser(tksua, suatenString, tuoiString, mkString, status);
						System.out.println(suaString);
					} else {
						System.out.println("ma tai khoan k ton tai");
					}
				}
			} else if (n == 4) {
				for (int i = 0; i < list.size(); i++) {
					System.out.print("ma tai khoan:" + list.get(i).getMaTaiKhoan() + ",");
					System.out.print("ten:" + list.get(i).getTen() + ",");
					System.out.print("tuoi:" + list.get(i).getTuoi() + ",");
					System.out.print("mat khau:" + list.get(i).getMatKhau() + ",");
					System.out.println("status:" + list.get(i).getSatatus());

				}

			} else if (n == 5) {
				scanner.nextLine();

				System.out.println("them ma nhom");
				String maNhom = scanner.nextLine();
				ArrayList<Nhom> list2 = new ArrayList<>();
				nhomdao sNhomdao=new nhomdao();
				list2=sNhomdao.laythongtinnhom();
				for (int i = 0; i < list2.size(); i++) {
					if (list2.get(i).getMaNhom().equals(maNhom))
						kt = 1;
				}
				if (kt == 1) {
					System.out.println("ma nhom trung vui long nhap lai");
				} else {
					int a=0;
					System.out.println("the ten nhom");
					String tenNhom = scanner.nextLine();
					for(int i=0;i<list2.size();i++) {
						if(list2.get(i).getTenNhom().equals(tenNhom)){
							a++;
						}
					}if(a<=10) {
					System.out.println("them ma tai khoan");
					int matk = scanner.nextInt();
					nhomdao ndNhomdao = new nhomdao();
					String themnhom = ndNhomdao.themNhom(maNhom, tenNhom, matk);
					System.out.println(themnhom);
					}
					else {
						System.out.println("nhom da day vui long lap nhom moi");
					}
				}
			} else if (n == 6) {
				scanner.nextLine();
				System.out.println("nhap ma nhom");
				String maNhom = scanner.nextLine();
				ArrayList<Nhom> list2 = new ArrayList<>();
				for (int i = 0; i < list2.size(); i++) {
					if (list2.get(i).getMaNhom().equals(maNhom)) {
						kt = 1;
					}
				}
				if (kt == 1) {
					scanner.nextLine();
					System.out.println("nhap ten nhom");
					String tenNhom = scanner.nextLine();
					System.out.println("nhap ma tai khoan");
					int matk = scanner.nextInt();
					nhomdao ndNhomdao = new nhomdao();
					String suaNhom = ndNhomdao.suaNhom(maNhom, tenNhom, matk);
					System.out.println(suaNhom);
				} else {
					System.out.println("ma nhom k ton tai");
				}
			} else if (n == 7) {
				scanner.nextLine();
				System.out.println("nhap ma nhom");
				String nhapMaNhom = scanner.nextLine();
				nhomdao sNhomdao = new nhomdao();
				ArrayList<Nhom> list2 = sNhomdao.laythongtinnhom();
				for (int i = 0; i < list2.size(); i++) {
					
					if (list2.get(i).getMaNhom().equals(nhapMaNhom)) {
						kt = 1;
					}
				}
				if (kt == 1) {
					nhomdao xNhomdao = new nhomdao();
					String xoaNhom = xNhomdao.xoaNhom(nhapMaNhom);
					System.out.println(xoaNhom);
				} else {
					System.out.println("ma nhom k ton tai");
				}

			} else if (n == 8) {
				scanner.nextLine();
				String nhapMaNhom = scanner.nextLine();
				nhomdao sNhomdao = new nhomdao();
				ArrayList<Nhom> list2 = sNhomdao.laythongtinnhom();
				for (int i = 0; i < list2.size(); i++) {
					System.out.print("ma nhom:" + list2.get(i).getMaNhom() + ",");
					System.out.print("ten Nhom:" + list2.get(i).getTenNhom() + ",");
					System.out.println("ma tai Khoan:" + list2.get(i).getMaTaiKhoan());
				}

			}
		} else if (check == 2) {
			ArrayList<ThuChi> list3 = new ArrayList<>();
			ThuChidao aChidao = new ThuChidao();
			list3 = aChidao.laythongtin();
			System.out.println("dang nhap thanh cong");
			System.out.println("nhan 1 de thu");
			System.out.println("nhan 2 de chi");
			int s = scanner.nextInt();

			String id_thuchi;

			if (s == 1) {
				scanner.nextLine();
				System.out.println("nhap noi dung viet theo dang:ten mataikhoan:, so tien thu :");
				id_thuchi = list3.size() + 1 + scanner.nextLine();
				int sotien = list3.get(list3.size() - 1).getSoTien();

				System.out.println("nhap so tien thu");

				int sotienthu = scanner.nextInt();
				sotien = sotien + sotienthu;
				// ma tai khoan
				int mataikhoan = nhapmakhoan;
				// ghi chu 1:thu
				int ghichu = 1;
				ThuChidao sChidao = new ThuChidao();

				String thu = sChidao.suathuchi(id_thuchi, sotien, mataikhoan, ghichu);
				System.out.println(thu);
				System.out.println(sotien);

			}
			if (s == 2) {

				int sotien = list3.get(list3.size() - 1).getSoTien();
				
				int ghichu = 2;
				scanner.nextLine();
				System.out.println("nhap noi dung chi");
				id_thuchi = list3.size() + 1 + scanner.nextLine();

				System.out.println("so tien chi");
				int sotienchi = scanner.nextInt();

				sotien = sotien - sotienchi;
				int mataikhoan = nhapmakhoan;
				String chiTien = aChidao.suathuchi(id_thuchi, sotien, mataikhoan, ghichu);
				System.out.println(chiTien);
				System.out.println(sotien);

			}else {
				System.out.println("vui long nhap lai 1:thu,:chi");
			}
			
		}else {
			System.out.println("dang nhap that bai");
		}

	}

}