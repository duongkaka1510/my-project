package bean;

public class Nhom {
	String maNhom;
	String tenNhom;
	int maTaiKhoan;
	public String getMaNhom() {
		return maNhom;
	}
	public void setMaNhom(String maNhom) {
		this.maNhom = maNhom;
	}
	public String getTenNhom() {
		return tenNhom;
	}
	public void setTenNhom(String tenNhom) {
		this.tenNhom = tenNhom;
	}
	public int getMaTaiKhoan() {
		return maTaiKhoan;
	}
	public void setMaTaiKhoan(int maTaiKhoan) {
		this.maTaiKhoan = maTaiKhoan;
	}
	public Nhom(String maNhom, String tenNhom, int maTaiKhoan) {
		super();
		this.maNhom = maNhom;
		this.tenNhom = tenNhom;
		this.maTaiKhoan = maTaiKhoan;
	}
	

}
