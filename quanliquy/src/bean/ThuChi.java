package bean;

public class ThuChi {
	String id_thuChi;
	int soTien;
	int maTaiKhoan;
	int ghiChu;
	public String getId_thuChi() {
		return id_thuChi;
	}
	public void setId_thuChi(String id_thuChi) {
		this.id_thuChi = id_thuChi;
	}
	public int getSoTien() {
		return soTien;
	}
	public void setSoTien(int soTien) {
		this.soTien = soTien;
	}
	public int getMaTaiKhoan() {
		return maTaiKhoan;
	}
	public void setMaTaiKhoan(int maTaiKhoan) {
		this.maTaiKhoan = maTaiKhoan;
	}
	public int getGhiChu() {
		return ghiChu;
	}
	public void setGhiChu(int ghiChu) {
		this.ghiChu = ghiChu;
	}
	public ThuChi(String id_thuChi, int soTien, int maTaiKhoan, int ghiChu) {
		super();
		this.id_thuChi = id_thuChi;
		this.soTien = soTien;
		this.maTaiKhoan = maTaiKhoan;
		this.ghiChu = ghiChu;
	}
	

}
