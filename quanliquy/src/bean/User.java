package bean;

public class User {
	int maTaiKhoan;
	String ten;
	String tuoi;
	String matKhau;
	int satatus;
	public int getMaTaiKhoan() {
		return maTaiKhoan;
	}
	public void setMaTaiKhoan(int maTaiKhoan) {
		this.maTaiKhoan = maTaiKhoan;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public String getTuoi() {
		return tuoi;
	}
	public void setTuoi(String tuoi) {
		this.tuoi = tuoi;
	}
	public String getMatKhau() {
		return matKhau;
	}
	public void setMatKhau(String matKhau) {
		this.matKhau = matKhau;
	}
	public int getSatatus() {
		return satatus;
	}
	public void setSatatus(int satatus) {
		this.satatus = satatus;
	}
	public User(int maTaiKhoan, String ten, String tuoi, String matKhau, int satatus) {
		super();
		this.maTaiKhoan = maTaiKhoan;
		this.ten = ten;
		this.tuoi = tuoi;
		this.matKhau = matKhau;
		this.satatus = satatus;
	}
	

}
